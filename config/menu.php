<?php

return [


    [
        "name" => "USER MANAGEMENT",
        "icon" => "-",
        "hasSub" => true,
        "link" => "#",
        "sub" => [
            [
                "permission" => "user-manage",
                "name" => "User",
                "route" => "user",
                "routeDet" => "index"
            ],
            [
                "permission" => "role-manage",
                "name" => "Hak Akses",
                "route" => "role",
                "routeDet" => "index"
            ],

            [
                "permission" => "permission-bumn",
                "name" => "Pegawai Bumn",
                "route" => "pegawaiBumn",
                "routeDet" => "index"
            ],

        ]

    ],
    [
        "name" => "TENAGA MEDIS",
        "icon" => "-",
        "hasSub" => true,
        "link" => "#",
        "sub" => [
            [
                "permission" => "candidate-manage",
                "name" => "Daftar Calon Tenaga Medis",
                "route" => "candidate",
                "routeDet" => "index"
            ],
            [
                "permission" => "dev-permission",
                "name" => "Tenaga Medis",
                "route" => "temp",
                "routeDet" => "index"
            ]

        ]

    ],

    [
        "name" => "CUSTOMERS",
        "icon" => "-",
        "hasSub" => true,
        "link" => "#",
        "sub" => [
            [
                "permission" => "customer-inquiry-manage",
                "name" => "Inquiry",
                "route" => "customerInquiry",
                "routeDet" => "index"
            ]

        ]

    ],

    [
        "name" => "PRODUCTS",
        "icon" => "-",
        "hasSub" => true,
        "link" => "#",
        "sub" => [
            [
                "permission" => "product-category",
                "name" => "Product Category",
                "route" => "productCategory",
                "routeDet" => "index"
            ],
            [
                "permission" => "product-manage",
                "name" => "Product Service",
                "route" => "products",
                "routeDet" => "index"
            ],


        ]

    ],


];
