<?php

namespace App\Http\Controllers\Back;


use App\Exceptions\CustomValidationException;
use App\Exceptions\ExecuteQueryException;
use App\Http\Controllers\Controller;
use Validator;

class BackController extends Controller
{
    protected $pageData = array(
        "title" => "My Health Care",
        "title-sub" => "",
        "title-display" => "",
        "meta-name" => "",
    );

    protected $routeName = "home";
    protected $viewPrefix = "home";

    protected $breadCrumb = array(
        "pageTitle" => "Dashboard", // yang masih kepake
        "bread" => null
    );

    public $response;

    public function __construct()
    {
        $this->response = new \stdClass();
        $this->response->status = true;
        $this->response->message = "";
        $this->logID = "User_ID";
    }

    private function  generatePageData(){
        $this->pageData["title-display"] = empty($this->pageData["title-sub"]) ? $this->pageData["title"] : $this->pageData["title"]." | ".$this->pageData["title-sub"];
    }

    protected function view($layout, $data= array()){
        $this->generatePageData();
        $data['pageData'] = $this->pageData;
        $data['breadCrumb'] = $this->breadCrumb;
        $data['routeName'] = $this->routeName;
        $data['viewPrefix'] = $this->viewPrefix;

        return view($layout, $data);
    }


    protected function customValidate($request, $exceptParams = array()){
        $validator  = ($this->ruleCustomMessages==null) ? Validator::make($request->all(), $this->rules) : Validator::make($request->all(), $this->rules, $this->ruleCustomMessages);
        if ($validator->fails()) {
            throw new CustomValidationException($validator->errors());
        }
        $input = $request->all();
        foreach ($input as $key => $item){
            if (!in_array($key, $exceptParams) && $item=="")
            {
                unset($input[$key]);
            }
        }
        return $input;
    }

    protected function throwExceptionQuery($msg){
        throw new ExecuteQueryException($msg);
    }

    public function ajax_response($data=null, $header= array(), $statusCode=200)
    {
        if ($data!=null) {
            $this->response->data = $data;
        }
        return \Response::json($this->response, $statusCode, $header);
    }

    #
    # get filter params
    #
    public function filterParams()
    {
        $filter = request()->get('filter');
        return empty($filter) ? array() : $filter;
    }
}