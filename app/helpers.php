<?php

if (! function_exists('checkMainMenu')) {
    function checkMainMenu($menu)
    {
        $host = url('/');
        $url = ($menu=='/') ? $host : url('/')."/".$menu;
        if($url == url()->current() ){
            echo "menu-item-active";
        }else{
            echo "";
        }
    }
}
/*
 * menu
 */
if (! function_exists('checkAdminMenu')) {
    function checkAdminMenu($arryRouteParent, $ret="", $retFalse="")
    {
        $currentRoute=\Route::currentRouteName();
        if($currentRoute==""){
            echo "";
        }else{
            $explodecurrentRoute = explode(".",$currentRoute);
            if (in_array($explodecurrentRoute[0], $arryRouteParent)){
                echo " ".$ret;
            }else{
                echo "".$retFalse;
            }
        }
    }
}

if (! function_exists('hasPermission')) {
    function hasPermission($arrayPermission)
    {
        $status=false;
        foreach ($arrayPermission as $permission){
            if(\Entrust::can(($permission))){
                $status=true;
                break;
            }
        }
        return $status;
    }
}

if (! function_exists('recursiveAccountDropDown')) {
    function recursiveAccountDropDown($accounts, $type, $tab="")
    {
        $tab .= "&nbsp;&nbsp;&nbsp;";
        return view('general.select.chart_of_account', compact('accounts', 'type', 'tab'));
    }
}

if (! function_exists('getDateHelper')) {
    function getDateHelper($format="Y-m-d")
    {
        return \Carbon\Carbon::now()->format($format);
    }
}


if (! function_exists('getMoneyFormat')) {
    function getMoneyFormat($money, $type = 'in')
    {
        return number_format($money,0,'.',',');
    }
}

if (! function_exists('getBulan')) {
    function getBulan($code)
    {
        $bulan = [
            "01" => "Januari",
            "02" => "Februari",
            "03" => "Maret",
            "04" => "April",
            "05" => "Mei",
            "06" => "Juni",
            "07" => "Juli",
            "08" => "Agustus",
            "09" => "September",
            "10" => "Oktober",
            "11" => "November",
            "12" => "Desember",
        ];
        return @$bulan[$code];
    }
}

if (! function_exists('__getListPermissions')) {
    function __getListPermissions($subMenu)
    {
        $result = [];
        foreach($subMenu as $value){
            $result[] = $value['permission'];
        }
        return $result;
    }
}

if (! function_exists('__getListRoutes')) {
    function __getListRoutes($subMenu)
    {
        $result = [];
        foreach($subMenu as $value){
            $result[] = $value['route'];
        }
        return $result;
    }
}