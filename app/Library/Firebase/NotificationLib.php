<?php
/**
 * Created by PhpStorm.
 * User: Vinra
 * Date: 4/1/2019
 * Time: 11:20 PM
 */

namespace App\Library\Firebase;


use App\Library\Common\Logger;
use App\Models\User;
use GuzzleHttp\Client;

class NotificationLib
{
    public static function pushNotification($deviceID, $title, $message, $type='admin'){
        try{
            $client = new Client();
            $firebase = \Config::get('firebase');

            $params = array(
                "to"=> $deviceID,
                "data" => array(
                    "title" => $title,
                    "type"  => $type,
                    "message"   => $message,
                )
            );

            $headers = [
                "Authorization" => "key=".$firebase['server_key'],
                "Content-Type" => "application/json",
            ];

            Logger::debug("serverKey: ".$firebase['server_key']);

            $response = $client->post($firebase['url'], ['headers' => $headers, 'body' => json_encode($params)]);
            $result = json_decode($response->getBody(), true);

            if (isset($result['success']) && $result['success']==1){
                Logger::debug("Berhasil Push notif");
            }else{
                Logger::error("GAGAL ".$response->getBody());
            }
        }catch (\Exception $e){
            Logger::error("Gagal Push Notification");
        }
    }


    public static function pushToPimpinan($title, $message){
        try{
            $users = User::join('karyawan', 'users.userable_id', '=', 'karyawan.id')
                ->where("users.userable_type", "karyawan")
                ->whereNotNull("users.device_id")
                ->where('jabatan_id', 2)->get();
            foreach ($users as $pimpinan){
                self::pushNotification($pimpinan->device_id, $title, $message, 'admin');
            }
        }catch (\Exception $e){
            Logger::error("Gagal mengambil data pimpinan");
        }

    }

    public static function pushNotifToKaryawan($karyawan_id, $title, $message){
        try{
            $user = User::where("users.userable_type", "karyawan")
                ->whereNotNull("users.device_id")
                ->where('users.userable_id', $karyawan_id)->first();
            if ($user){
                self::pushNotification($user->device_id, $title, $message, 'karyawan');
            }else{
                Logger::debug("karaywan dengan id".$karyawan_id. " tidak ditemukan");
            }
        }catch (\Exception $e){
            Logger::error("Gagal mengambil data karyawan untuk push notif");
        }
    }
}