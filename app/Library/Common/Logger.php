<?php
namespace App\Library\Common;

class Logger extends \Log{
    public static $id;

    public static function generateId() {
        Timer::startTimer();
        return static::$id = uniqid ("", true);
    }

    public static function setID($id){
        return static::$id = $id;
    }

    public static function formatMessage($message) {
        return static::getId() . "\t" . $message."\t[".Timer::getTempTime()."s]\n";
    }

    public static function getId(){
        if(!empty(static::$id))
            return static::$id;
        return self::generateId();
    }

    public static function info($message) {
        return parent::info ( self::formatMessage ( $message ) );
    }

    public static function error($message) {
        return parent::error ( self::formatMessage ( $message ) );
    }

    public static function debug($message) {
        return parent::debug ( self::formatMessage ( $message ) );
    }

    public static function warning($message){
        return parent::warning ( self::formatMessage ( $message ) );
    }


}