<?php
namespace App\Library\Common;

class Timer {
    public static $timeStart;
    public static $timeFinish;
    public static $tempTimeStart;

    /*
     * Set start time
     */
    public static function startTimer() {
        self::$timeStart = microtime ( true );
        self::$tempTimeStart = microtime ( true );
    }

    /*
     * set finish time
     */
    public static function finishTimer() {
        self::$timeFinish = microtime ( true );
    }

    /*
     * get execution time from startTimer()
     */
    public static function getTimeSpend() {
        self::finishTimer();
        return number_format ( self::$timeFinish - self::$timeStart, 4 );
    }

    /*
     * Set temporary execution time
     */
    public static function setTempTimeStart(){
        self::$tempTimeStart = microtime ( true );
    }

    /*
     * Get execution time each Log action
     */
    public static function getTempTime(){
        $finish = microtime(true);
        $time = number_format ( $finish - self::$tempTimeStart, 4 );
        self::setTempTimeStart();
        return $time;
    }
}