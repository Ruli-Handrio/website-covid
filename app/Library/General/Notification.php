<?php
/**
 * Created by PhpStorm.
 * User: Aryo
 * Date: 20/02/19
 * Time: 0:54
 */

namespace App\Library\General;

use Carbon\Carbon;
use GuzzleHttp\Client;

class Notification
{

    public static function sendNotification($to, $message)
    {

        $dataMessage = array(
            "to" => $to,
            "notification" => array(
                "sound" => "default",
                "body" => Carbon::now()->format('d M Y'),
                "title" => $message,
                "content_available" => true,
                "priority" => "high",
                "image" => "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Logo_BUMN_Untuk_Indonesia_2020.svg/1200px-Logo_BUMN_Untuk_Indonesia_2020.svg.png",
            ),
            "data"  => array(
                "sound" => "default",
                "content" => Carbon::now()->format('d M Y'),
                "image" => "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Logo_BUMN_Untuk_Indonesia_2020.svg/1200px-Logo_BUMN_Untuk_Indonesia_2020.svg.png",
                "title" => $message,
                "content_available" => true,
                "priority" => "high",
            ),
        );


        $requestContent = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'key=AAAAmK8jCKI:APA91bE7TOFQ7bUQWjbCn2n8FIjsmxBGX_Iq23N5cn-Qr7CezCSs58mZI2kiUUstLdm_ctqIWhtFBXRRe2pVPNB04feMa7lqANgiz3DZYoASJ2_aeo_joVyt3P5hcmi7hA-xD6syUFMc'
            ],
            'json' => $dataMessage
        ];

        try {
            $client = new Client();

            $apiRequest = $client->request('POST', 'https://fcm.googleapis.com/fcm/send', $requestContent);

            $response = json_decode($apiRequest->getBody());

        } catch (\Exception $re) {
            // For handling exception.
        }
    }

}