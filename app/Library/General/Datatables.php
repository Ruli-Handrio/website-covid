<?php
/**
 * Created by PhpStorm.
 * User: Aryo
 * Date: 20/02/19
 * Time: 0:54
 */

namespace App\Library\General;

class Datatables
{

    public static function getDataTablesData($res, $filter, $mapColumn = array()){

        $all = $res->count();

        if (isset($filter['order'][0])){
            $columnIndex = (int)$filter['order'][0]['column'];
            $columnOrder = $filter['columns'][$columnIndex]['name'];
            $direction = $filter['order'][0]['dir'];

            $columnOrder = isset($mapColumn[$direction]) ? $mapColumn[$direction] : $columnOrder;
            $res->orderBy($columnOrder, $direction);
        }

        if ($filter['length'] != -1)
            $res->limit($filter['length'])->offset($filter['start']);

        $result = array(
            'recordsTotal' => $all,
            "recordsFiltered" => $all,
            "data" => $res->get(),
            "input" => $filter
        );

        return $result;
    }

}
