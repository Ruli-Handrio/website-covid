<?php
namespace App\Traits;

use Carbon\Carbon;

trait ValetTrait
{
    protected $workDays = array(
        "1" => "Monday",
        "2" => "Tuesday",
        "3" => "Wednesday",
        "4" => "Thursday",
        "5" => "Friday",
    );
    protected function generateCode($objectModel, $atrribute, $length=8, $prefix='')
    {
        $result = $objectModel->where($atrribute, 'LIKE', $prefix.'%')->max($atrribute);
        $prefixLen = strlen($prefix);
        $subPrefix = substr(trim($result), $prefixLen);
        return $prefix.(str_pad((int)$subPrefix+1, $length-$prefixLen, "0", STR_PAD_LEFT));
    }

    protected function getDateTime()
    {
        return Carbon::now();
    }

    protected function getDate($format = 'Y-m-d')
    {
        $dtime= Carbon::now();
        return $dtime->format($format);
    }

    protected function getHumanDateTime($dateTime)
    {
        $dtime = Carbon::createFromFormat('Y-m-d H:i:s', $dateTime);
        return $dtime->format('M d, Y h:i A');
    }

    protected function getHumanDate($dateTime)
    {
        $dtime = Carbon::createFromFormat('Y-m-d', $dateTime);
        return $dtime->format('M d, Y');
    }

    protected function terbilang($number){
        $x = abs($number);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($number <12) {
            $temp = " ". $angka[$number];
        } else if ($number <20) {
            $temp = $this->terbilang($number - 10). " belas";
        } else if ($number <100) {
            $temp = $this->terbilang($number/10)." puluh". $this->terbilang($number % 10);
        } else if ($number <200) {
            $temp = " seratus" . $this->terbilang($number - 100);
        } else if ($number <1000) {
            $temp = $this->terbilang($number/100) . " ratus" . $this->terbilang($number % 100);
        } else if ($number <2000) {
            $temp = " seribu" . $this->terbilang($number - 1000);
        } else if ($number <1000000) {
            $temp = $this->terbilang($number/1000) . " ribu" . $this->terbilang($number % 1000);
        } else if ($number <1000000000) {
            $temp = $this->terbilang($number/1000000) . " juta" . $this->terbilang($number % 1000000);
        } else if ($number <1000000000000) {
            $temp = $this->terbilang($number/1000000000) . " milyar" . $this->terbilang(fmod($number,1000000000));
        } else if ($number <1000000000000000) {
            $temp = $this->terbilang($number/1000000000000) . " trilyun" . $this->terbilang(fmod($number,1000000000000));
        }
        return $temp;
    }

    protected function getUserLogin(){
        return \Auth::user();
    }

}
