<?php

namespace App\Console\Commands\Dev;

use App\Models\ModulGroup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use App\Models\Modul;
use App\Models\Permission;
use App\Models\Role;
use Maatwebsite\Excel\Facades\Excel;

class DevelopmentPermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'development:permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Permission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (\App::environment('production', 'staging'))
        {
            //ie("Maneh Kernaon plok?  I'm on production or staging\n");
        }

        echo "Command : php artisan config:cache\n";
        Artisan::call("config:cache");
        echo Artisan::output();

        echo "------------------------------\n\n\n";
        echo "Command : php artisan config:cache\n";
        echo "Update Modul Group....\n";
        $file = "/files/data/ModulGroups.xlsx";
        try {
            $status=true;
            $data = array();
            $result = Excel::load($file, function ($reader) {
                return $reader;
            });

            $kosong=0;
            foreach($result->all() as $key =>  $value){
                if($kosong==3){
                    break;
                }
                if($value->name==null){
                    $kosong++;
                }else{
                    $data[] = array(
                        'id'=> trim($value->id),
                        'name'=> $value->name,
                        'description' => $value->description,
                    );
                }
            }

            \DB::beginTransaction();
            try {
                foreach ($data as $item){
                    $modulGroup = ModulGroup::find($item['id']);
                    if($modulGroup){
                        $modulGroup->update($item);
                    }else{
                        $modulGroup= ModulGroup::create($item);
                    }
                }
                \DB::commit();
                echo "Update Modul Group berhasil !\n";
            } catch (\Exception $e) {
                echo "Update Modul Group Gagal\n";
                dd($e->getMessage());
            }

        }catch (\Exception $e) {
            echo "Migrate Data Gagal\n";
            dd($e->getMessage());
        }

        echo "=============================================\n\n";
        echo "Update Modul....\n";
        $file = "/files/data/Moduls.xlsx";
        try {
            $status=true;
            $data = array();
            $result = Excel::load($file, function ($reader) {
                return $reader;
            });

            $kosong=0;
            foreach($result->all() as $key =>  $value){
                if($kosong==3){
                    break;
                }
                if($value->name==null){
                    $kosong++;
                }else{
                    $data[] = array(
                        'id'=> trim($value->id),
                        'name'=> $value->name,
                        'description' => $value->description,
                        'modul_group_id' => $value->modul_group_id,
                    );
                }
            }

            \DB::beginTransaction();
            try {
                foreach ($data as $item){
                    $modul = Modul::find($item['id']);
                    if($modul){
                        $modul->update($item);
                    }else{
                        $modul= Modul::create($item);
                    }
                }
                \DB::commit();
                echo "Update Modul berhasil !\n";
            } catch (\Exception $e) {
                echo "Update Modul Gagal\n";
                dd($e->getMessage());
            }

        }catch (\Exception $e) {
            echo "Migrate Data Gagal\n";
            dd($e->getMessage());
        }

        echo "=============================================\n\n";
        echo "Update Permission....\n";

        $file = "/files/data/Permissions.xlsx";
        try {
            $status=true;
            $data = array();
            $result = Excel::load($file, function ($reader) {
                return $reader;
            });
//            dd($result->all());
            $kosong=0;
            foreach($result->all() as $key =>  $value){
//                dd($value);
                if($kosong==3){
                    break;
                }
                if($value->name==null){
                    $kosong++;
                }else{
                    $data[] = array(
                        'name'=> trim($value->name),
                        'display_name'=> $value->display_name,
                        'description' => $value->description,
                        'modul_id' => $value->modul_id,
                        'role_id' => $value->role_id,
                        'is_default' => $value->is_default,
                    );
                }
            }

            \DB::beginTransaction();
            try {
//                $superadmin=Role::where('name', 'superadmin')->first();
                foreach ($data as $item){
                    $permission = Permission::where('name', $item['name'])->first();
                    if($permission){
                        $permission->update($item);
                    }else{
                        $permission = Permission::create($item);
                        $role=Role::find($item['role_id']);
                        $role->attachPermission($permission);
//                        $superadmin->attachPermission($permission);

//                        $arrRoleId = explode(",", $item['role_id']);
//                        foreach ($arrRoleId as $roleID){
//                            $roleID = (int)trim($roleID);
//                            $role=Role::find($roleID);
//                            if($superadmin->id!=$role->id){
//                                $role->attachPermission($permission);
//                            }
//                        }
                    }
                }
                \DB::commit();
                echo "Update Permission berhasil !\n";
            } catch (\Exception $e) {
                echo "Update Permission Gagal\n";
                dd($e->getMessage());
            }

        }catch (\Exception $e) {
            echo "Migrate Data Gagal\n";
            dd($e->getMessage());
        }
    }
}
