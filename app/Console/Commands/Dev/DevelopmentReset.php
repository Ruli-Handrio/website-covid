<?php

namespace App\Console\Commands\Dev;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DevelopmentReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'development:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jalanin artisan migrate::refresh, db:seed, development:permission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (\App::environment('production', 'staging'))
        {
            die("Maneh Kernaon plok?  I'm on production or staging\n");
        }

        echo "Command : php artisan migrate:refresh\n";
        Artisan::call("migrate:refresh");
        echo Artisan::output();

        echo "------------------------------\n\n\n";

        echo "Command : php artisan db:seed\n";
        Artisan::call("db:seed");
        echo Artisan::output();

        echo "------------------------------\n\n\n";
        echo "Command : php artisan development:permission\n";
        Artisan::call("development:permission");
        echo Artisan::output();

        echo "Well Done, Boss!\n";

    }


}
