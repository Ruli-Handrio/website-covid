<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::auth();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', ['as'=>'home.home','uses'=>'Front\HomeController@home']);

Route::group(['middleware' => ['auth']], function () {
//    Route::get('/', ['as'=>'home.home','uses'=>'HomeController@home']); //
    Route::get('/home', ['as'=>'home.home','uses'=>'HomeController@home']); //

    Route::group(['prefix' => 'account'], function () {
        Route::get('profile',['as'=>'profile.index','uses'=>'ProfileController@index']);
        Route::get('profile/getContent/{content}',['as'=>'profile.getContent','uses'=>'ProfileController@getContent']);
        Route::post('profile/changePassword',['as'=>'profile.changePassword','uses'=>'ProfileController@changePassword']);


        Route::post('profile',['as'=>'profile.update','uses'=>'ProfileController@update']);
    });


    Route::group(['prefix' => 'user-management'], function () {
        Route::get('users',['as'=>'user.index','uses'=>'UserController@index','middleware' => ['permission:user-manage']]);
        Route::get('users/create',['as'=>'user.create','uses'=>'UserController@create','middleware' => ['permission:user-manage']]);
        Route::post('users/create',['as'=>'user.store','uses'=>'UserController@store','middleware' => ['permission:user-manage']]);
        Route::get('users/{id}/edit',['as'=>'user.edit','uses'=>'UserController@edit','middleware' => ['permission:user-manage']]);
        Route::put('users/{id}',['as'=>'user.update','uses'=>'UserController@update','middleware' => ['permission:user-manage']]);
        Route::delete('users/{id}',['as'=>'user.destroy','uses'=>'UserController@destroy','middleware' => ['permission:user-manage']]);

        Route::get('roles',['as'=>'role.index','uses'=>'RoleController@index','middleware' => ['permission:role-manage']]);
        Route::get('roles/create',['as'=>'role.create','uses'=>'RoleController@create','middleware' => ['permission:role-manage']]);
        Route::post('roles/create',['as'=>'role.store','uses'=>'RoleController@store','middleware' => ['permission:role-manage']]);
        Route::get('roles/{id}/edit',['as'=>'role.edit','uses'=>'RoleController@edit','middleware' => ['permission:role-manage']]);
        Route::put('roles/{id}',['as'=>'role.update','uses'=>'RoleController@update','middleware' => ['permission:role-manage']]);
        Route::delete('roles/{id}',['as'=>'role.destroy','uses'=>'RoleController@destroy','middleware' => ['permission:role-manage']]);
    });

    Route::group(['prefix' => 'partner'], function () {
        Route::get('candidates',['as'=>'candidate.index','uses'=>'CandidatesController@index','middleware' => ['permission:candidate-manage']]);
        Route::get('candidates/{id}',['as'=>'candidate.show','uses'=>'CandidatesController@show','middleware' => ['permission:candidate-manage']]);

        //Route::get('candidates/create',['as'=>'candidate.create','uses'=>'CandidatesController@create','middleware' => ['permission:dev-permission']]);
        //Route::post('candidates/create',['as'=>'candidate.store','uses'=>'CandidatesController@store','middleware' => ['permission:dev-permission']]);
        //Route::get('candidates/{id}/edit',['as'=>'candidate.edit','uses'=>'CandidatesController@edit','middleware' => ['permission:dev-permission']]);
        //Route::put('candidates/{id}',['as'=>'candidate.update','uses'=>'CandidatesController@update','middleware' => ['permission:dev-permission']]);
        Route::delete('candidates/{id}',['as'=>'candidate.destroy','uses'=>'CandidatesController@destroy','middleware' => ['permission:candidate-manage']]);
    });

    Route::group(['prefix' => 'customers'], function () {
        Route::get('inquiry',['as'=>'customerInquiry.index','uses'=>'CustomerInquiryController@index','middleware' => ['permission:customer-inquiry-manage']]);
        Route::get('inquiry/create',['as'=>'customerInquiry.create','uses'=>'CustomerInquiryController@create','middleware' => ['permission:customer-inquiry-manage']]);
        Route::post('inquiry/create',['as'=>'customerInquiry.store','uses'=>'CustomerInquiryController@store','middleware' => ['permission:customer-inquiry-manage']]);
        Route::get('inquiry/{id}/edit',['as'=>'customerInquiry.edit','uses'=>'CustomerInquiryController@edit','middleware' => ['permission:customer-inquiry-manage']]);
        Route::put('inquiry/{id}',['as'=>'customerInquiry.update','uses'=>'CustomerInquiryController@update','middleware' => ['permission:customer-inquiry-manage']]);

        Route::get('inquiry/{id}/activities',['as'=>'customerInquiry.activities','uses'=>'CustomerInquiryController@activities','middleware' => ['permission:customer-inquiry-manage']]);
        Route::post('inquiry/{id}/activities/create',['as'=>'customerInquiry.storeActivity','uses'=>'CustomerInquiryController@storeActivity','middleware' => ['permission:customer-inquiry-manage']]);

        //Route::get('inquiry/{id}',['as'=>'customerInquiry.show','uses'=>'CustomerInquiryController@show','middleware' => ['permission:customer-inquiry-manage']]);
        //Route::delete('inquiry/{id}',['as'=>'customerInquiry.destroy','uses'=>'CustomerInquiryController@destroy','middleware' => ['permission:customer-inquiry-manage']]);

    });

    Route::group(['prefix' => 'services'], function () {

        Route::get('product-categories',['as'=>'productCategory.index','uses'=>'ProductCategoryController@index','middleware' => ['permission:product-category']]);
        Route::get('product-categories/create',['as'=>'productCategory.create','uses'=>'ProductCategoryController@create','middleware' => ['permission:product-category']]);
        Route::post('product-categories/create',['as'=>'productCategory.store','uses'=>'ProductCategoryController@store','middleware' => ['permission:product-category']]);
        Route::get('product-categories/{id}/edit',['as'=>'productCategory.edit','uses'=>'ProductCategoryController@edit','middleware' => ['permission:product-category']]);
        Route::put('product-categories/{id}',['as'=>'productCategory.update','uses'=>'ProductCategoryController@update','middleware' => ['permission:product-category']]);
        Route::delete('product-categories/{id}',['as'=>'productCategory.destroy','uses'=>'ProductCategoryController@destroy','middleware' => ['permission:product-category']]);

        Route::get('products',['as'=>'products.index','uses'=>'ProductsController@index','middleware' => ['permission:product-manage']]);
        Route::get('products/create',['as'=>'products.create','uses'=>'ProductsController@create','middleware' => ['permission:product-manage']]);
        Route::post('products/create',['as'=>'products.store','uses'=>'ProductsController@store','middleware' => ['permission:product-manage']]);
        Route::get('products/{id}/edit',['as'=>'products.edit','uses'=>'ProductsController@edit','middleware' => ['permission:product-manage']]);
        Route::post('products/{id}',['as'=>'products.update','uses'=>'ProductsController@update','middleware' => ['permission:product-manage']]);
        Route::delete('products/{id}',['as'=>'products.destroy','uses'=>'ProductsController@destroy','middleware' => ['permission:product-manage']]);
        Route::get('products/viewImage/{imageName}',['as'=>'products.viewImage','uses'=>'ProductsController@viewImage','middleware' => ['permission:product-manage']]);

    });


    Route::get('temp',['as'=>'temp.index','uses'=>'TempController@index','middleware' => ['permission:dev-permission']]);

});

